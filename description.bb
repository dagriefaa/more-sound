[img=https://i.imgur.com/oipVR6D.png][/img]
                                                      [b][url=https://gitlab.com/dagriefaa/more-sound]GitLab Repository[/url][/b]

Adds sound pack support and some extra sounds that aren't in the base game.

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2913469777][img]https://i.imgur.com/XGCiJRy.png[/img][/url]

[h1]Features[/h1]
◼ Breaking noises for blocks (that can break)
◼ Collision noises for blocks (that don't have them)
◼ Burning noises
◼ Customisation of all block sounds via soundpacks
◼ Full multiplayer support

[h1]Sound Packs[/h1]
The mod comes with three integrated soundpacks - default, nothing, and metal. 
An option is provided in the 🔧 Keymapper tool to set the soundpack AND to apply the set soundpack to other blocks with the same skin.

To install user-created soundpacks, unzip them into [b]Besiege/Besiege_data/Mods/Data/MoreSound_.../[/b].
Links to such soundpacks will be provided if/when people make them.

If a soundpack isn't installed, loading a machine that uses them will not automatically wipe that soundpack from the machine. It will only change when you change it.

...

Instructions to create soundpacks are provided in [url=https://gitlab.com/dagriefaa/more-sound/-/blob/main/README.md?ref_type=heads#usage]the GitLab repository[/url].

...

Default sounds are from freesound.org (or are variations on them) and the vanilla game.