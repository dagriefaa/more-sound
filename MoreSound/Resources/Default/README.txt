To use this soundpack, place the folder in `Besiege/Besiege_data/Mods/Data/MoreSound_.../`. It must be unzipped.

---

To edit this soundpack, see the README at https://gitlab.com/dagriefaa/more-sound#creating-soundpacks.

IMPORTANT - Check the licensing of the sounds you add (e.g. Creative Commons 0) and follow it! If you don't know, assume you can't use it at all.
