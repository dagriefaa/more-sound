﻿using ObjectExplorer.Mappings;
using UnityEngine;

namespace MoreSound;
internal class OEMapper : MonoBehaviour {

    void Awake() {
        ObjectExplorer.ObjectExplorer.AddMappings(Mod.MOD_NAME,
            new MDictionary<SoundPackService, string, SoundPack>("Packs", c => SoundPackService.Packs),
            new MComponent<BlockSoundController, BlockBehaviour>(nameof(BlockSoundController.Block), c => c.Block),
            new MString<BlockSoundController>(nameof(BlockSoundController.SoundPack), c => c.SoundPack.Value)
        );
        Destroy(this);
    }
}
