﻿using Besiege.UI;
using Modding;
using Modding.Blocks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace MoreSound;
internal class AudioService : MonoBehaviour {

    public static AudioService Instance;

    public const float ROLLOFF_MIN = 3.71f;
    public const int ROLLOFF_MAX = 500;

    public static AudioClip Fire;

    static readonly Dictionary<AudioSource, Coroutine> FadeCoroutines = new();

    static MessageType MVBreakMessage;

    // mixers
    static AudioMixerGroup _physicsMixer = null;
    static AudioMixerGroup _underwaterPhysicsMixer = null;
    public static AudioMixerGroup PhysicsMixer { get => _physicsMixer ??= ReferenceMaster.GetMixer("Physics"); }
    public static AudioMixerGroup UnderwaterPhysicsMixer { get => _underwaterPhysicsMixer ??= ReferenceMaster.GetWaterMixerFrom(_physicsMixer); }

    // main camera
    static Transform _mainCameraTransform = null;
    public static Transform MainCameraTransform {
        get {
            if (!_mainCameraTransform && Camera.main) {
                _mainCameraTransform = Camera.main.transform;
            }
            return _mainCameraTransform;
        }
    }

    void Awake() {
        Instance = this;

        // setup networking
        MVBreakMessage = ModNetworking.CreateMessageType(DataType.Block);
        ModNetworking.Callbacks[MVBreakMessage] += m => {
            var block = (Block)m.GetData(0);
            block.InternalObject.GetComponent<BlockSoundController>().PlayBreak();
        };

        Make.OnReady(Mod.MOD_NAME, () => {
            AudioService.Fire = ModResource.GetAudioClip("Fire");
            AudioService.Fire.name = "Fire";
        });
    }

    // if adding more events, don't add more methods, replace this one with a generic thing
    public static void SendBreakMessage(BlockBehaviour block) {
        ModNetworking.SendToAll(MVBreakMessage.CreateMessage(Block.From(block)));
    }

    public static AudioSource SetupAudioSource(AudioSource source, AudioClip clip, float volume = 1f, float pitch = 1f, bool looop = false) {
        source.outputAudioMixerGroup = PhysicsMixer;
        source.rolloffMode = AudioRolloffMode.Logarithmic;
        source.minDistance = ROLLOFF_MIN;
        source.maxDistance = ROLLOFF_MAX;
        source.spatialBlend = 1;
        source.priority = 128;
        source.clip = clip;
        source.pitch = pitch;
        source.volume = volume;
        source.loop = looop;
        source.playOnAwake = false; // why is this enabled on start...?
        return source;
    }

    public static AudioSource SetupAudioSource(AudioSource source, SoundPack.ClipDef def, bool looop = false) {
        return SetupAudioSource(source, def.Clip, def.Volume, def.Pitch, looop);
    }

    public static bool UpdateFading(AudioSource source, bool active, bool wasActive, float volume, float speed = 3) {
        if (wasActive != active) {
            if (active) {
                Instance.FadeIn(source, volume, speed);
            }
            else {
                Instance.FadeOut(source, speed);
            }
        }
        return active;
    }

    public static void UpdateMixer(BlockBehaviour b, AudioSource s) {
        if (!s.isPlaying) { return; }
        s.outputAudioMixerGroup = (b.submergedPercent < 0.9f) ? PhysicsMixer : UnderwaterPhysicsMixer;
    }

    public void FadeIn(AudioSource source, float targetVolume, float speed) {
        if (FadeCoroutines.TryGetValue(source, out var coroutine)) {
            if (coroutine != null) {
                StopCoroutine(coroutine);
            }
            FadeCoroutines.Remove(source);
        }
        FadeCoroutines.Add(source, StartCoroutine(IEFadeIn(source, targetVolume, speed)));
    }

    IEnumerator IEFadeIn(AudioSource source, float targetVolume, float speed) {
        if (!source) {
            FadeCoroutines.Remove(source);
            yield break;
        }
        if (FadeCoroutines.TryGetValue(source, out var coroutine)) {
            StopCoroutine(coroutine);
        }
        if (!source.isPlaying) {
            source.volume = 0;
            if (Mathf.Approximately(source.time, 0)) {
                source.Play();
                source.time = Random.Range(0.0f, source.clip.length);
            }
            else {
                source.UnPause();
            }
        }
        while (source && source.volume < targetVolume - 0.01f) {
            source.volume = Mathf.Lerp(source.volume, targetVolume, Time.deltaTime * speed);
            yield return null;
        }
        FadeCoroutines.Remove(source);
    }

    public void FadeOut(AudioSource source, float speed) {
        if (FadeCoroutines.TryGetValue(source, out var coroutine)) {
            if (coroutine != null) {
                StopCoroutine(coroutine);
            }
            FadeCoroutines.Remove(source);
        }
        FadeCoroutines.Add(source, StartCoroutine(IEFadeOut(source, speed)));
    }

    IEnumerator IEFadeOut(AudioSource source, float speed) {
        while (source && source.volume > 0.01f) {
            source.volume = Mathf.Lerp(source.volume, 0, Time.deltaTime * speed);
            yield return null;
        }
        if (source) {
            source.Pause();
        }
        FadeCoroutines.Remove(source);
    }
}
