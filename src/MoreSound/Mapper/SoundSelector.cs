﻿using Besiege.UI;
using Besiege.UI.Bridge;
using Besiege.UI.Bridge.Extensions;
using Besiege.UI.Serialization;
using Modding.Mapper;
using UnityEngine;
using UnityEngine.UI;

namespace MoreSound.Mapper;
internal class SoundSelector : CustomSelector<string, MSound> {

    static GameObject Prefab;

    bool isUpdating = false;

    Option soundMenu;
    Image soundMenuImage;
    Text soundMenuText;

    BlockBehaviour Block => CustomMapperType.Block;
    string Value => CustomMapperType.Value;

    protected override void CreateInterface() {
        Background.localScale = new Vector3(4.5f, 0.5f, 1);

        if (!Prefab) {
            Prefab = Make.LoadProject(Mod.MOD_NAME, nameof(SoundSelector), this.transform).GetComponentInParent<Canvas>().gameObject;
            Prefab.SetActive(false);
            Prefab.transform.SetParent(Mod.ModControllerObject.transform, false);
        }

        var go = Prefab.Copy(this.transform);
        go.gameObject.SetActive(true);
        Project project = go.GetComponentInChildren<Project>();
        project.RebuildTransformList();

        project["PaintButton"].GetComponent<Button>().onClick.AddListener(
            () => {
                SoundPackService.ApplySoundPackToBlocksWithSameSkin(Block);
                BlockMapper.AudioSource.Play();
            }
        );

        soundMenu = project["SoundOption"].GetComponent<Option>();
        soundMenuImage = soundMenu.transform.Find("Label/Icon").GetComponent<Image>();
        soundMenuText = soundMenu.transform.Find("Label/Text").GetComponent<Text>();
        soundMenu.options = CustomMapperType.SoundPacks;
        soundMenu.onValueChanged.AddListener(i => {
            if (!isUpdating) {
                CustomMapperType.Value = CustomMapperType.SoundPacks[i];
                BlockMapper.AudioSource.Play();
                OnEdit();
            }
            soundMenuImage.sprite = SoundPackService.IconFor(Value);
        });

        UpdateInterface();
    }

    protected override void UpdateInterface() {
        if (!this) { return; } // bodge fix bc mapper change handler is not cleared on destroy???

        isUpdating = true;

        var soundIndex = soundMenu.options.IndexOf(Value);
        if (soundIndex >= 0) {
            soundMenu.Index = soundIndex;
        }
        else { // soundpack not installed
            soundMenuText.text = Value;
            soundMenuImage.sprite = SoundPackService.IconFor(Value);
        }

        isUpdating = false;
    }
}