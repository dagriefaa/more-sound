﻿using Modding.Mapper;
using System.Collections.Generic;

namespace MoreSound.Mapper;
internal class MSound : MCustom<string> {

    public BlockBehaviour Block { get; }
    public List<string> SoundPacks { get; }

    public MSound(BlockBehaviour block, List<string> soundPacks) : base("", "SoundPack", SoundPackService.DEFAULT_PACK) {
        this.Block = block;
        this.SoundPacks = soundPacks;
    }

    public override XData SerializeValue(string value) => new XString(SerializationKey, value);
    public override string DeSerializeValue(XData data) => (data as XString).Value;
}
