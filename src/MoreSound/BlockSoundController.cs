﻿using Modding.Blocks;
using MoreSound.Definitions;
using MoreSound.Mapper;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MoreSound;
public class BlockSoundController : MonoBehaviour {

    const float COLLISION_THRESHOLD_SQR = 6f;
    const int COLLIDE_PRIORITY = 126;

    static readonly HashSet<BlockType> SILENT_BLOCKS = new() {
        BlockType.BuildNode,
        BlockType.BuildEdge,
        BlockType.Pin,
        BlockType.Unused,
        BlockType.CameraBlock,
        BlockType.Magnet,
        BlockType.RopeMeasure,
    };

    public BlockBehaviour Block;
    internal MSound SoundPack { get; private set; }
    bool CanMakeFireNoise => Block.fireTag && Block.fireTag.fireControllerCode && Block is not TimedRocket;

    IAction ActionDef = null;
    SoundPack.ClipDef BreakDef = null;
    SoundPack.ClipDef CollideDef = null;

    AudioSource FireSource;
    AudioSource BreakAndCollideSource;
    SoundOnCollide CollideHandler = null;

    new Transform transform; // cache to increase performance on hot paths

    bool onFire = false;
    bool playingBreakSound = false;

    public static void AddToBlock(Block b) {
        if (b.InternalObject.isSimulating || SILENT_BLOCKS.Contains(b.InternalObject.Prefab.Type)) {
            return;
        }
        b.InternalObject.gameObject.AddComponent<BlockSoundController>();
    }

    public void PlayBreak() {
        if (!BreakAndCollideSource) {
            return;
        }
        BreakAndCollideSource.Stop();
        BreakAndCollideSource.priority = 100;
        AudioService.SetupAudioSource(BreakAndCollideSource, BreakDef.Clip, 0.3f * BreakDef.Volume, BreakDef.Pitch + Random.Range(-0.5f, 0.5f));
        AudioService.UpdateMixer(Block, BreakAndCollideSource);
        BreakAndCollideSource.PlayDelayed(Random.Range(0f, 0.2f));
        playingBreakSound = true;
    }

    void Awake() {
        Block = GetComponent<BlockBehaviour>();
        var packs = SoundPackService.Packs
            .Where(kvp => SoundPackService.IsDefaultPack(kvp.Key)
                || kvp.Value.VanillaBlocks.ContainsKey(Block.Prefab.Type))
            .Select(kvp => kvp.Key)
            .ToList();
        SoundPack = Block.AddCustom(new MSound(Block, packs)) as MSound;
        transform = Block.transform;
    }

    void Start() {
        if (!Block.isSimulating) {
            return;
        }

        if (CanMakeFireNoise) { // rocket emitter shouldn't make noise
            FireSource = AudioService.SetupAudioSource(this.gameObject.AddComponent<AudioSource>(), AudioService.Fire, looop: true);
        }

        // configure collision sound
        CollideDef = SoundPackService.CollideSoundFor(Block.BuildingBlock, SoundPack.Value);
        if (CollideDef != null) {

            if (Block is BuildSurface bs) {
                bs.currentType.impactSfx = new[] { CollideDef.Clip };
            }
            else {
                CollideHandler = GetComponent<SoundOnCollide>();

                if (!CollideHandler) {
                    var rsc = GetComponent<RandomSoundController>() ?? gameObject.AddComponent<RandomSoundController>(); // rsc audio 1 is not used by block action sounds so we can safely use it
                    rsc.audioSource ??= this.gameObject.AddComponent<AudioSource>();
                    CollideHandler = gameObject.AddComponent<SoundOnCollide>();
                    CollideHandler.basicInfo = Block;
                    CollideHandler.randSoundController = rsc;
                    CollideHandler.cutoff = COLLISION_THRESHOLD_SQR;
                }
                CollideHandler.randSoundController.randomPitch = true;
                CollideHandler.randSoundController.audioclips = new[] { CollideDef.Clip };
                CollideHandler.randSoundController.pitchRange = (Block is CrossBowBlock) ? 0.05f : 0.75f; // fix crossbow shooting sound being cursed

                BreakAndCollideSource = CollideHandler.randSoundController.audioSource;
            }
        }

        // configure break sound
        BreakDef = SoundPackService.BreakSoundFor(Block.BuildingBlock, SoundPack.Value);
        if (BreakDef != null) {
            var breakComponent = GetComponent<FragmentVisualController>();
            if (breakComponent) {
                breakComponent.breakSfx = new AudioClip[0];
                BreakAndCollideSource ??= breakComponent.audio; // reuse component; note existing collision sounds will also share this source
            }
            if (Block is BuildSurface surface) {
                if (surface.FragmentController) {
                    var breakForce = surface.Joints?[0]?.breakForce ?? 10000f;
                    surface.FragmentController.OnBreak += () => OnJointBreak(breakForce);
                }
                surface.currentType.breakSfx = new AudioClip[0];
                BreakAndCollideSource ??= surface.sfx; // ditto
            }
            BreakAndCollideSource ??= this.gameObject.AddComponent<AudioSource>();
        }

        if (BreakAndCollideSource) {
            if (CollideDef != null) {
                AudioService.SetupAudioSource(BreakAndCollideSource, CollideDef.Clip, 0.2f * CollideDef.Volume, 1.25f * CollideDef.Pitch);
            }
            else {
                AudioService.SetupAudioSource(BreakAndCollideSource, BreakDef.Clip); // will setup on break
            }
            BreakAndCollideSource.priority = COLLIDE_PRIORITY; // set to higher priority than action sounds
        }

        // configure action
        ActionDef = SoundPackService.MakeActionComponentFor(Block, SoundPack.Value);
    }

    void Update() {
        if (!Block.isSimulating) {
            return;
        }

        // update fire
        if (CanMakeFireNoise) {
            onFire = AudioService.UpdateFading(FireSource, Block.fireTag.fireControllerCode.enabled, onFire, 0.3f);
            AudioService.UpdateMixer(Block, FireSource);
            FireSource.priority = Mathf.Clamp(200 + Mathf.RoundToInt(0.01f * (AudioService.MainCameraTransform.position - transform.position).sqrMagnitude), 0, 255);
        }

        // setup collide after break sound finishes
        if (playingBreakSound && CollideDef != null && !BreakAndCollideSource.isPlaying) {
            AudioService.SetupAudioSource(BreakAndCollideSource, CollideDef.Clip, 0.2f * CollideDef.Volume, 1.25f * CollideDef.Pitch);
            playingBreakSound = false;
            BreakAndCollideSource.priority = COLLIDE_PRIORITY; // set to higher priority than action sounds
        }

        ActionDef?.UpdatePlaying();
    }

    void OnJointBreak(float breakForce) {
        if (breakForce < AudioService.ROLLOFF_MAX) {
            return;
        }
        if (!StatMaster.isClient) {
            AudioService.SendBreakMessage(Block);
        }
        PlayBreak();
    }
}
