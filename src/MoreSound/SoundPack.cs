﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MoreSound;
[Serializable]
public class SoundPack {

    class ClipDefConverter : JsonConverter {
        public override bool CanConvert(Type objectType) => throw new NotImplementedException();
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            JToken token = JToken.Load(reader);

            // "Magic.ogg"                                      -> Dictionary<string, ClipDef> { { "", new ClipDef { File = "Magic" } } }
            // { "File": "Magic.ogg", ... }                     -> Dictionary<string, ClipDef> { { "", new ClipDef { File = "Magic", ... } } }
            // { "Magic": "Magic.ogg", ... }                    -> Dictionary<string, ClipDef> { { "Magic", new ClipDef { File = "Magic" }, ... } }
            // { "Magic": { "File": "Magic.ogg", ... }, ... }   -> Dictionary<string, ClipDef> { { "Magic", new ClipDef { File = "Magic", ... }, ... } }

            if (token.Type == JTokenType.String) {
                return new Dictionary<string, ClipDef> { { "", new ClipDef { File = token.ToString() } } };
            }
            else if (token["File"] != null) {
                return new Dictionary<string, ClipDef> { { "", token.ToObject<ClipDef>(serializer) } };
            }
            else {
                var result = new Dictionary<string, ClipDef>();
                foreach (var prop in token) {
                    if (prop.First.Type == JTokenType.String) {
                        result.Add(prop.Path, new ClipDef { File = prop.First.ToString() });
                    }
                    else {
                        result.Add(prop.Path, prop.First.ToObject<ClipDef>(serializer));
                    }
                }
                return result;
            }
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => serializer.Serialize(writer, value);
    }

    [Serializable]
    public class ClipDef {
        public string File;
        public float Volume = 1f;
        public float Pitch = 1f;

        [JsonIgnore]
        public AudioClip Clip;

        public override string ToString() {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    [Serializable]
    public class SoundDef {

        [JsonConverter(typeof(ClipDefConverter))]
        public Dictionary<string, ClipDef> Break;
        [JsonConverter(typeof(ClipDefConverter))]
        public Dictionary<string, ClipDef> Collide;
        [JsonConverter(typeof(ClipDefConverter))]
        public Dictionary<string, ClipDef> Action;

        public override string ToString() {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public string Icon;

    [JsonIgnore]
    public Sprite IconSprite;

    public Dictionary<BlockType, SoundDef> VanillaBlocks;

    public override string ToString() {
        return JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}
