﻿using Besiege.UI;
using Besiege.UI.Extensions;
using Modding;
using MoreSound.Actions;
using MoreSound.Definitions;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MoreSound;
internal class SoundPackService : MonoBehaviour {
    public static SoundPackService Instance { get; private set; }

    const string TEMPLATE_PACK = "Template";

    public const string DEFAULT_PACK = "Default";
    public const string MUTE_PACK = "No Sound";
    public const string EMPTY_KEY = "";

    static readonly string[] AUDIO_FILES = new string[] { ".aif", ".wav", ".mp3", ".ogg" };

    // block config constants
    const float SURFACE_LIGHT = 0.5f;
    const float SURFACE_MID = 2f;
    const float OVERFLOW_POWER = 2.1474836E+09f;

    static Sprite missingIcon;

    public static Dictionary<string, SoundPack> Packs { get; private set; } = new();

    public static bool IsDefaultPack(string packName) => packName is DEFAULT_PACK;

    public static bool PackHasBlock(string packName, BlockType type) => IsDefaultPack(packName) || (Packs.Get(packName)?.VanillaBlocks.ContainsKey(type) ?? false);

    public static Sprite IconFor(string packName) => Packs.Get(packName)?.IconSprite ?? missingIcon;

    /// <returns>
    /// sound definition for given block and soundpack <br/>
    /// - with fallback to default if not implemented <br/>
    /// - or null if not present in either
    /// </returns>
    public static SoundPack.ClipDef BreakSoundFor(BlockBehaviour b, string packName)
        => Maybe.Of(b switch {
            BuildSurface bs => bs.material.Value switch {
                0 => bs.Rigidbody.mass switch {
                    < SURFACE_LIGHT => "WoodLight",
                    < SURFACE_MID => "WoodMid",
                    _ => "WoodHeavy"
                },
                1 => bs.Rigidbody.mass switch {
                    < SURFACE_LIGHT => "GlassLight",
                    < SURFACE_MID => "GlassMid",
                    _ => "GlassHeavy"
                },
                _ => "Other"
            },
            _ => ""
        }).Map(key => RetrieveClipWithFallback(def => def.Break, b, packName, key))
        .OrElse(() => null);

    /// <returns>
    /// sound definition for given block and soundpack <br/>
    /// - with fallback to default if not implemented <br/>
    /// - or null if not present in either
    /// </returns>
    public static SoundPack.ClipDef CollideSoundFor(BlockBehaviour b, string packName)
        => Maybe.Of(b switch {
            BuildSurface bs => bs.material.Value switch {
                0 => bs.Rigidbody.mass switch {
                    < SURFACE_LIGHT => "WoodLight",
                    < SURFACE_MID => "WoodMid",
                    _ => "WoodHeavy"
                },
                1 => bs.Rigidbody.mass switch {
                    < SURFACE_LIGHT => "GlassLight",
                    < SURFACE_MID => "GlassMid",
                    _ => "GlassHeavy"
                },
                _ => "Other"
            },
            _ => ""
        })
        .Map(key => RetrieveClipWithFallback(def => def.Collide, b, packName, key))
        .OrElse(() => null);

    /// <returns>
    /// sound definition for given block and soundpack <br/>
    /// - with fallback to default if not implemented <br/>
    /// - or null if not present in either
    /// </returns>
    public static SoundPack.ClipDef ActionSoundFor(BlockBehaviour b, string packName, string key = EMPTY_KEY)
        => RetrieveClipWithFallback(def => def.Action, b, packName, key);

    /// <summary>
    /// create and stand up component for block
    /// </summary>
    public static IAction MakeActionComponentFor(BlockBehaviour block, string packName)
        => block switch { { Prefab.Type: BlockType.StartingBlock } => new ActionExisting<BlockBehaviour>(block, packName, (b, c) => {
            AudioSource audioSource = b.GetComponent<AudioSource>();
            if (audioSource) { // todo - check later if this null check is actually necessary for client
                audioSource.clip = c;
            }
        }),
            FlyWheelBlock flywheel => new ActionExisting<FlyWheelBlock>(flywheel, packName, new() {
                { "Lock", (b, c) => b.lockSfx = c },
                { "Unlock", (b, c) => b.unlockSfx = c },
            }),

            CogMotorControllerHinge motor => new ActionExisting<CogMotorControllerHinge>(motor, packName, (b, c) => Maybe.Of(b.sfx).Apply(src => src.clip = c)),
            SteeringWheel steering => new ActionExisting<SteeringWheel>(steering, packName, (b, c) => b.sfx.clip = c),
            SliderCompress piston => new ActionExisting<SliderCompress>(piston, packName, (b, c) => b.sfx.clip = c),
            CanonBlock cannon => new ActionExisting<CanonBlock>(cannon, packName, (b, c) => b.audioSource.clip = c),
            PropellorController prop => new ActionExisting<PropellorController>(prop, packName, (b, c) => b.sfx.clip = c),
            BalloonController balloon => new ActionExisting<BalloonController>(balloon, packName, (b, c) => b.snapAudio.clip = c),
            SpringReleaseBlock jaw => new ActionExisting<SpringReleaseBlock>(jaw, packName, (b, c) => b.impactSfx = new[] { c }),
            HarpoonController harpoon => new ActionExisting<HarpoonController>(harpoon, packName, (b, c) => b.sfx.clip = c),
            TimedRocket rocket => new ActionExisting<TimedRocket>(rocket, packName, (b, c) => {
                b.flightSFX.clip = c;
                b.flightSFX.loop = true;
            }),
            LogicGate gate => new ActionExisting<LogicGate>(gate, packName, (b, c) => b.sfx.clip = c),
            TimerBlock timer => new ActionExisting<TimerBlock>(timer, packName, (b, c) => b.sfx.clip = c),
            DragBlock drag => new ActionExisting<DragBlock>(drag, packName, (b, c) => b.sfx.clip = c),

            FlamethrowerController flamethrower => new ActionExisting<FlamethrowerController>(flamethrower, packName, new() {
                { "Ignition", (b, c) => b.fireIgnite = c },
                { "Fire", (b, c) => b.fireLoop = c },
                { "Bubbles", (b, c) => b.bubbleLoop = c },
            }),
            CrossBowBlock crossbow => new ActionExisting<CrossBowBlock>(crossbow, packName, new() {
                { "Shot", (b, c) => b.randomSoundController.audioclips2 = new[]{c} },
                { "Empty", (b, c) => b.randomSoundController.audioclips3 = new[]{c} },
            }),
            GrabberBlock grabber => new ActionExisting<GrabberBlock>(grabber, packName, new() {
                { "Grab", (b, c) => b.joinOnTriggerBlock.grabSound = c },
                { "Drop", (b, c) => b.joinOnTriggerBlock.dropSound = c },
            }),
            WaterCannonController water => new ActionExisting<WaterCannonController>(water, packName, new() {
                { "Water", (b, c) => {
                    if (b.StrengthSlider.Value is >= 0 and < OVERFLOW_POWER) { b.waterClip = c; }
                } },
                { "Steam", (b, c) => {
                    if (b.StrengthSlider.Value is >= 0 and < OVERFLOW_POWER) { b.steamClip = c; }
                } },
                { "Magic", (b, c) => {
                    if (b.StrengthSlider.Value is < 0 or >= OVERFLOW_POWER) { b.steamClip = b.waterClip = c; }
                } },
            }),
            FlyingController spiral => new ActionExisting<FlyingController>(spiral, packName, new() {
                {"Slow", (b, c) =>{ if (Mathf.Abs(spiral.SpeedSlider.Value) is >= 0 and <= 2) { b.sfx.clip = c; }}},
                {"Fast", (b, c) => {if (Mathf.Abs(spiral.SpeedSlider.Value) is > 2 and < 1E20f) { b.sfx.clip = c; }}},
                {"Magic", (b, c) => {if (Mathf.Abs(spiral.SpeedSlider.Value) >= 1E20f) { b.sfx.clip = c; }}},
            }),

            VacuumBlock vacuum => new ActionVacuum(vacuum, packName),
            SpringCode winch => new ActionWinch(winch, packName),
            ExplosiveBolt decoupler => new ActionDecoupler(decoupler, packName),

            // NauticalScrew screw => new ActionScrew(screw, packName),
            // RudderController rudder => new ActionRudder(rudder, packName),

            _ => null
        };

    static SoundPack.ClipDef RetrieveClipWithFallback(Func<SoundPack.SoundDef, Dictionary<string, SoundPack.ClipDef>> getClip, BlockBehaviour block, string packName, string key)
        => Maybe.Of(Packs.Get(packName))
            .Map(pack => pack.VanillaBlocks.Get(block.Prefab.Type))
            .Map(getClip)
            .Map(actions => actions.Get(key))
            .OrElse(IsDefaultPack(packName)
                ? () => null
                : () => RetrieveClipWithFallback(getClip, block, DEFAULT_PACK, key));


    public static void ApplySoundPackToBlocksWithSameSkin(BlockBehaviour originalBlock) {
        var blocks = Machine.Active().BuildingBlocks
            .Where(block => block.VisualController.selectedSkin.pack.id == originalBlock.VisualController.selectedSkin.pack.id)
            .ToList();
        BlockMapper.EditField(blocks, originalBlock, originalBlock.GetComponent<BlockSoundController>().SoundPack);
    }

    void Awake() {
        Instance = this;

        Make.OnReady(Mod.MOD_NAME, () => {
            missingIcon = ModResource.GetTexture("UnknownIcon").Texture.ToSprite();
        });

        // builtin soundpacks
        StartCoroutine(IELoad("Resources/Default", false, p => Packs.Add(DEFAULT_PACK, p)));
        StartCoroutine(IELoad("Resources/No Sound", false, p => Packs.Add(MUTE_PACK, p)));
        StartCoroutine(IELoad("Resources/Metal", false, p => Packs.Add("Metal", p)));

        // user soundpacks
        string[] folders = Modding.ModIO.GetDirectories("/", true);

        if (!folders.Any(TEMPLATE_PACK.Equals)) {
            Modding.ModIO.CreateDirectory(TEMPLATE_PACK, true);
            foreach (var file in Modding.ModIO.GetFiles("Resources/Default", false)) {
                var fileName = file.Split('/').Last();
                Modding.ModIO.WriteAllBytes($"{TEMPLATE_PACK}/{fileName}", Modding.ModIO.ReadAllBytes(file, false), true);
            }
            Debug.Log("Created template sound pack in data folder", this);
        }

        foreach (var folder in folders) {
            if (folder == TEMPLATE_PACK) {
                continue;
            }
            StartCoroutine(IELoad(folder, true, p => Packs.Add(folder, p)));
        }
    }

    IEnumerator IELoad(string folderPath, bool isDataFolder, Action<SoundPack> onManifestRead) {

        void LoadAndValidate(Dictionary<string, SoundPack.ClipDef> dict, string what, Dictionary<string, ModAudioClip> clips, string packName, BlockType block) {
            if (dict == null) {
                return;
            }
            List<string> keysToRemove = new();
            foreach (var kvp in dict) {
                // ignore null/empty
                if (string.IsNullOrEmpty(kvp.Value.File)) {
                    keysToRemove.Add(kvp.Key);
                    continue;
                }
                var key = $"{packName}.{kvp.Value.File}";

                // warn if invalid
                if (!clips.TryGetValue(key, out var clip)) {
                    Debug.LogWarning($"BlockType {block}.{what}[{kvp.Key}] references {kvp.Value.File} which was not loaded - removing", this);
                    keysToRemove.Add(kvp.Key);
                    continue;
                }
                clip.AudioClip.name = clip.Name;
                kvp.Value.File = key;
                kvp.Value.Clip = clip;
            }
            keysToRemove.ForEach(x => dict.Remove(x));
        }

        Debug.Log($"Loading sound pack {folderPath}", this);
        System.Diagnostics.Stopwatch watch = new();
        watch.Start();

        var packName = folderPath.Split('/').Last();

        // load resources
        var paths = Modding.ModIO.GetFiles(folderPath, isDataFolder).ToList();

        var manifest = Modding.ModIO.ReadAllText(paths.First(x => x.EndsWith("manifest.json")), isDataFolder);
        var soundPack = JsonConvert.DeserializeObject<SoundPack>(manifest);

        // validate json loaded
        if (soundPack.VanillaBlocks == null || soundPack.VanillaBlocks.Count == 0) {
            Debug.LogError($"Failed to load manifest for sound pack {folderPath}", this);
            yield break;
        }
        soundPack.VanillaBlocks.Where(x => x.Value == null).ToList().ForEach(x => Debug.LogWarning($"Sound definition invalid for block type {x.Key} in sound pack {folderPath}", this));

        // register pack
        onManifestRead(soundPack);
        Debug.Log($"Registered sound pack {folderPath} in {watch.ElapsedMilliseconds}ms", this);

        var completedJobs = 0;
        var totalJobs = 0;

        // load icon
        if (Modding.ModIO.ExistsFile($"{folderPath}/{soundPack.Icon}", isDataFolder)) {
            var iconTex = ModResource.CreateTextureResource($"{packName}.Icon", $"{folderPath}/{soundPack.Icon}", isDataFolder);
            totalJobs += 1;
            if (iconTex.Loaded) {
                soundPack.IconSprite = iconTex.Texture.ToSprite();
                completedJobs += 1;
            }
            else {
                iconTex.OnLoad += () => {
                    soundPack.IconSprite = iconTex.Texture.ToSprite();
                    completedJobs += 1;
                };
            }
        }

        // load audio files
        var clips = new Dictionary<string, ModAudioClip>();
        foreach (var path in paths) {
            if (!AUDIO_FILES.Any(path.EndsWith)) {
                continue;
            }
            var name = $"{packName}.{path.Split('/').Last()}";
            var clip = ModResource.CreateAudioClipResource(name, path, isDataFolder);
            clips.Add(name, clip);
            totalJobs += 1;
            if (clip.Loaded) {
                completedJobs += 1;
            }
            else {
                clip.OnLoad += () => {
                    completedJobs += 1;
                };
            }
        }

        while (completedJobs < totalJobs) {
            yield return new WaitForEndOfFrame();
        }

        // validate and set clips in data
        foreach (var dataKvp in soundPack.VanillaBlocks) {
            var def = dataKvp.Value;
            LoadAndValidate(def.Collide, nameof(def.Collide), clips, packName, dataKvp.Key);
            LoadAndValidate(def.Break, nameof(def.Break), clips, packName, dataKvp.Key);
            LoadAndValidate(def.Action, nameof(def.Action), clips, packName, dataKvp.Key);
        }

        watch.Stop();
        Debug.Log($"Loaded sound pack {folderPath} in {watch.ElapsedMilliseconds}ms", this);
    }
}
