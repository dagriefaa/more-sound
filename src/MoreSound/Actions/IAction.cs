﻿namespace MoreSound.Definitions;
public interface IAction {
    void UpdatePlaying();
}
