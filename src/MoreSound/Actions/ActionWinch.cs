﻿using MoreSound.Definitions;
using UnityEngine;

namespace MoreSound.Actions;
internal class ActionWinch : IAction {

    SpringCode winch;
    AudioSource source;

    float volume = 1f;
    bool wasPlaying = false;

    public ActionWinch(SpringCode winch, string packName) {
        if (!winch.winchMode) {
            return; // springs exist
        }
        var sound = SoundPackService.ActionSoundFor(winch, packName);
        this.winch = winch;
        source = AudioService.SetupAudioSource(winch.gameObject.AddComponent<AudioSource>(), sound, true);
        source.pitch = Mathf.Clamp(Mathf.Abs(winch.SpeedSlider.Value), 0.0f, 1.5f);
        volume = 0.1f * sound.Volume;
    }

    public void UpdatePlaying() {
        if (!winch) {
            return; // not configured
        }
        bool contractHeld = winch.ContractKey.IsHeld || winch.ContractKey.EmulationHeld(true);
        bool unwindHeld = winch.UnwindKey.IsHeld || winch.UnwindKey.EmulationHeld(true);

        bool playing = contractHeld ^ unwindHeld;
        wasPlaying = AudioService.UpdateFading(source, playing, wasPlaying, volume, 8);
        if (source.isPlaying) {
            AudioService.UpdateMixer(winch, source);
        }
    }
}
