﻿using Besiege.UI.Extensions;
using MoreSound.Definitions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MoreSound.Actions;
internal class ActionExisting<T> : IAction where T : BlockBehaviour {

    public ActionExisting(T b, string packName, Action<T, AudioClip> setClip, string clipKey = SoundPackService.EMPTY_KEY)
        => Setup(b, packName, setClip, clipKey);

    public ActionExisting(T b, string packName, Dictionary<string, Action<T, AudioClip>> clipSetters) {
        foreach (var kvp in clipSetters) {
            Setup(b, packName, kvp.Value, kvp.Key);
        }
    }

    void Setup(T b, string packName, Action<T, AudioClip> setClip, string clipKey = "")
        => Maybe.Of(SoundPackService.ActionSoundFor(b, packName, clipKey))
            .Apply(sound => setClip(b, sound.Clip));

    public void UpdatePlaying() {
        // nothing
    }
}
