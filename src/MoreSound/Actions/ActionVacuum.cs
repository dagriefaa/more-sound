﻿using MoreSound.Definitions;
using UnityEngine;

namespace MoreSound.Actions;
internal class ActionVacuum : IAction {

    AudioSource source;
    VacuumBlock vacuumBlock;
    VacuumController vacuumController;
    float volume = 1f;

    bool wasPlaying = false;

    public ActionVacuum(VacuumBlock vacuum, string packName) {
        var sound = SoundPackService.ActionSoundFor(vacuum, packName);
        this.vacuumBlock = vacuum;
        this.vacuumController = vacuum.GetComponent<VacuumController>();
        source = AudioService.SetupAudioSource(vacuum.gameObject.AddComponent<AudioSource>(), sound, true);
        volume = 0.3f * sound.Volume;
    }

    public void UpdatePlaying() {
        var playing = !vacuumController.isOff;
        wasPlaying = AudioService.UpdateFading(source, playing, wasPlaying, volume);
        if (source.isPlaying) {
            AudioService.UpdateMixer(vacuumBlock, source);
        }
    }
}
