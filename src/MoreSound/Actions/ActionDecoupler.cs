﻿using MoreSound.Definitions;
using UnityEngine;

namespace MoreSound.Actions;
internal class ActionDecoupler : IAction {

    ExplosiveBolt Block;
    ConfigurableJoint Joint;
    AudioSource Source;

    bool wasJoint;

    public ActionDecoupler(ExplosiveBolt block, string packName) {
        var sound = SoundPackService.ActionSoundFor(block, packName);
        if (sound == null) {
            return;
        }
        Block = block;
        Source = AudioService.SetupAudioSource(block.gameObject.AddComponent<AudioSource>(), sound.Clip, sound.Volume, sound.Pitch);
        Joint = block.GetComponent<ConfigurableJoint>();
        wasJoint = Joint;
    }

    public void UpdatePlaying() {
        if (Source && !Joint && wasJoint) {
            AudioService.UpdateMixer(Block, Source);
            Source.volume *= 8250f * 0.00002f;
            Source.Play();
            wasJoint = false;
        }
    }
}
