using Besiege.UI;
using Besiege.UI.Serialization;
using Modding;
using Modding.Mapper;
using MoreSound.Mapper;
using System;
using UnityEngine;

namespace MoreSound;
public class Mod : ModEntryPoint {

    public const string MOD_NAME = "MoreSound";

    public static GameObject ModControllerObject;

    public override void OnLoad() {
        // Init common mod controller object
        ModControllerObject = GameObject.Find("ModControllerObject");
        if (!ModControllerObject) { UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject")); }

        // Load Object Explorer mappings
        if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
            ModControllerObject.AddComponent<OEMapper>();
        }

        // UIFactory
        Make.RegisterSerialisationProvider(Mod.MOD_NAME, new SerializationProvider {
            CreateText = c => Modding.ModIO.CreateText(c),
            GetFiles = c => Modding.ModIO.GetFiles(c),
            ReadAllText = c => Modding.ModIO.ReadAllText(c),
            AllResourcesLoaded = () => ModResource.AllResourcesLoaded,
            OnAllResourcesLoaded = e => ModResource.OnAllResourcesLoaded += e
        });

        ModControllerObject.AddComponent<AudioService>();
        ModControllerObject.AddComponent<SoundPackService>();

        Events.OnBlockInit += BlockSoundController.AddToBlock;
        Make.OnReady(Mod.MOD_NAME, CustomMapperTypes.AddMapperType<string, MSound, SoundSelector>);
    }

    public static bool SceneNotPlayable() {
        return !AdvancedBlockEditor.Instance;
    }
}
