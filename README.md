<div align="center">
<img src="header.png" alt="More Sound" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=2073422152<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game.

![Steam Views](https://img.shields.io/steam/views/2073422152?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/2073422152?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/2073422152?color=%231b2838&logo=steam&style=for-the-badge)
</div><br>

More Sound is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds soundpacks and fills in a few missing sounds (like breaking sounds).

The mod is tuned to fit the game's existing sound design. Mostly.

## Features
- Breaking sounds for blocks (that can break)
- Collision sounds for blocks (that don't have them)
- Action sounds for blocks that don't have action sounds
- Burning noises
- Customisation for all block sounds via soundpacks
- Full multiplayer support

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 12 (ish) / .NET Framework 3.5

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2019 (or later).
4. Press F6 to compile the mod.

## Usage
For basic usage, no configuration is required.

### Installing/Using Soundpacks
The mod comes with three integrated soundpacks - default, nothing, and metal. An option is provided in the 🔧 Keymapper tool to set the soundpack AND to apply the set soundpack to other blocks with the same skin.

To install user-created soundpacks, unzip them into `Besiege/Besiege_data/Mods/Data/MoreSound_.../`.

If a soundpack isn't installed, loading a machine that uses them will not automatically wipe that soundpack from the machine. It will only change when you change it.

### Creating Soundpacks
To create a soundpack, copy or rename the template soundpack in `Besiege/Besiege_data/Mods/Data/MoreSound_.../`.

Soundpacks consist of a folder containing a manifest file, audio clips, and an icon. 

Audio clips can be the following types: .aif, .wav, .mp3, .ogg
- All clips in the folder will be loaded regardless of whether the pack uses them or not, so remove sounds you don't end up using.
  
The manifest file contains mappings of provided clips to block sounds. 
- The template manifest contains all supported sounds. Adding more stuff *might* work but is not supported.
- Most sounds can either be defined as just a filename, or as an object containing the filename, pitch, and volume.
    - Many action sounds do not support this functionality.
- Only vanilla blocks are currently supported.
- If an error occurs while loading the soundpack (e.g. invalid filename), it will be logged in the console as an error (Ctrl + K).
```json
// Example of how to define a sound
"Collide": "CollideSound.ogg"
"Break": {
    "File": "BreakSound.ogg",
    "Volume": 2,
    "Pitch": 1.2,
}
```

## Status
This mod is feature-complete. Additional features may be added in the future as required.

Features that are **out of scope** for this mod:
- Ambient noise (e.g. wind with high speed)
- Sonic booms
- Changing of existing vanilla sounds (beyond what's already been changed)
- Adding mechanical sounds to unpowered joints (hinges, swivels, unpowered wheels, etc)
- Music

## Credits
Default sounds are from freesound.org (or are variations on them) and the vanilla game.